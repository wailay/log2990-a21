# LOG2990-A21
```
master	        develop 		  features(individuel)
  |	  	        feature1     	         |
  |	  	           |		            f11
  |	  	           |                     |
  |	  	           |                    f12
  |	  	           |                     |
  |	  	           | 		             |
  |	  	           * <------------------ |
  |	  	         feature2	            f21	
  |	  	           | 	                 |
  |	               |		             |
  |   	           |    		        f22
  |	  	           |    	             |
  |	  	           * <------------------ |		     
sprint 1 <---------|		             .
  |		           .		             .
  |		           .		             .
  |		           .		
  |
  |
  |
  |
sprint 2
  |
  |
  |
  |
  |
  |
sprint 3
```

* One **master branch** grouping the general sprints
* The **dev branch** will regroup commits from features
* Each new features will be coded in a new feature branch that will later be merged to dev.

example steps for a git workflow.

**THESE INITIAL STEPS ARE TO BE DONE ONLY ONCE AT REPO CREATION**

    1) Create dev branch and sync it with master

    git branch dev
    git push -u origin dev

    2) Clone repo and checkout to dev

    git clone <repo>
    git checkout dev

**START DEVELOPING A FEATURE**

1) Create a new branch that will track "dev" and give it a brief name describing your feature

        git checkout -b <feature_name> dev

2) Make sure you are up to date with the dev branch

        git pull origin dev

3) Write your code for the new feature and make small incremental commits

        * code
        git add <files>
        git commit -m "message"
        ** always make sure to pull frequently from dev to make sure you are not behind commits
        git pull origin dev

4) when done coding the feature ( and made sure there are no conflicts with git pull origin dev )

        git push --set-upstream origin <feature_name>

        * The command line should give you a link to create a pull request, make sure to change the base branch from master to dev
        * also select the options to delete your branch, so the repository wont be flooded with feature branches

5) You can then delete you branch if not needed (good practice)

        git branch -D <feature_name>

6) After your pull request was approved and merged do not forget to pull your dev branch

        git checkout dev
        git pull

7) when the sprint is done, merge dev to master

        git push -u origin dev


https://nvie.com/posts/a-successful-git-branching-model/

